/** @file EventSaverAIDA.cxx
 *  @brief EventSaverAIDA class implementation
 *
 *  @author Douglas Davis < ddavis@cern.ch >
 */

#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"
#include "TopConfiguration/TopConfig.h"

#include "xAODTruth/xAODTruthHelpers.h"
#include "MCTruthClassifier/MCTruthClassifierDefs.h"

#include "EventSaverAIDA/EventSaverAIDA.h"

ClassImp(top::EventSaverAIDA)

static const SG::AuxElement::ConstAccessor<int> truthType      {"truthType"};
static const SG::AuxElement::ConstAccessor<int> truthOrigin    {"truthOrigin"};
static const SG::AuxElement::ConstAccessor<int> bkgTruthOrigin {"bkgTruthOrigin"};

static const SG::AuxElement::ConstAccessor<unsigned int> particleOrigin          {"particleOrigin"};
static const SG::AuxElement::ConstAccessor<unsigned int> classifierParticleOrigin{"classifierParticleOrigin"};

static const SG::AuxElement::ConstAccessor<
  ElementLink<xAOD::TruthParticleContainer>
  > truthParticleLink{"truthParticleLink"};

top::EventSaverAIDA::EventSaverAIDA() : top::EventSaverFlatNtuple(),
                                        m_config(nullptr),
                                        m_lumiBlock(0),
                                        m_met_sumet(0.0),
                                        m_el_truthMatched(),
                                        m_el_true_pdg(),
                                        m_el_true_pt(),
                                        m_el_true_eta(),
                                        m_mu_truthMatched(),
                                        m_mu_true_pdg(),
                                        m_mu_true_pt(),
                                        m_mu_true_eta(),
                                        m_nu_pt(),
                                        m_nu_eta(),
                                        m_nu_phi(),
                                        m_nu_origin()
{}

top::EventSaverAIDA::~EventSaverAIDA() {}

StatusCode top::EventSaverAIDA::initialize() { return StatusCode::SUCCESS; }

void top::EventSaverAIDA::initialize(std::shared_ptr<top::TopConfig> config, TFile* file,
                                     const std::vector<std::string>& extraBranches) {

  // Base class must be called
  top::EventSaverFlatNtuple::initialize(config, file, extraBranches);
  m_config = config;

  // Add output to trees
  for (auto systematicTree : treeManagers()) {
    systematicTree->makeOutputVariable(m_lumiBlock, "lbn");
    if ( m_config->isMC() ) {
      systematicTree->makeOutputVariable(m_el_truthMatched,   "el_truthMatched");
      systematicTree->makeOutputVariable(m_el_true_pdg,       "el_true_pdg");
      systematicTree->makeOutputVariable(m_el_true_pt,        "el_true_pt");
      systematicTree->makeOutputVariable(m_el_true_eta,       "el_true_eta");
      systematicTree->makeOutputVariable(m_mu_truthMatched,   "mu_truthMatched");
      systematicTree->makeOutputVariable(m_mu_true_pdg,       "mu_true_pdg");
      systematicTree->makeOutputVariable(m_mu_true_pt,        "mu_true_pt");
      systematicTree->makeOutputVariable(m_mu_true_eta,       "mu_true_eta");
    }
    systematicTree->makeOutputVariable(m_met_sumet, "met_sumet");
  }

  if ( topConfig()->doTopParticleLevel() ) {
    top::EventSaverFlatNtuple::setupParticleLevelTreeManager();
    particleLevelTreeManager()->makeOutputVariable(m_nu_pt,    "nu_pt");
    particleLevelTreeManager()->makeOutputVariable(m_nu_eta,   "nu_eta");
    particleLevelTreeManager()->makeOutputVariable(m_nu_phi,   "nu_phi");
    particleLevelTreeManager()->makeOutputVariable(m_nu_origin,"nu_origin");
  }
}


void top::EventSaverAIDA::saveEvent(const top::Event& event) {
  bool isSim = top::isSimulation(event);
  if ( !isSim ) m_lumiBlock = event.m_info->lumiBlock();

  m_met_sumet = event.m_met->sumet();

  if ( m_config->useElectrons() ) {
    m_el_truthMatched.resize(event.m_electrons.size());
    m_el_true_pdg.resize(event.m_electrons.size());
    m_el_true_pt.resize(event.m_electrons.size());
    m_el_true_eta.resize(event.m_electrons.size());

    unsigned int i(0);
    for ( const auto* elPtr : event.m_electrons ) {
      if ( top::isSimulation(event) ) {
        int true_type      = 0;
        int true_origin    = 0;
        int true_originbkg = 0;
        if ( truthType.isAvailable(*elPtr) )      true_type      = truthType(*elPtr);
        if ( truthOrigin.isAvailable(*elPtr) )    true_origin    = truthOrigin(*elPtr);
        if ( bkgTruthOrigin.isAvailable(*elPtr) ) true_originbkg = bkgTruthOrigin(*elPtr);

        m_el_truthMatched[i] = ( true_type == MCTruthPartClassifier::ParticleType::IsoElectron ||
                                 ( true_type == MCTruthPartClassifier::ParticleType::BkgElectron &&
                                   true_origin == MCTruthPartClassifier::ParticleOrigin::PhotonConv &&
                                   ( true_originbkg == MCTruthPartClassifier::ParticleOrigin::TauLep ||
                                     true_originbkg == MCTruthPartClassifier::ParticleOrigin::ZBoson ||
                                     true_originbkg == MCTruthPartClassifier::ParticleOrigin::WBoson ) ) );
        m_el_true_pdg[i] = 0;
        m_el_true_pt[i]  = 0;
        m_el_true_eta[i] = 0;
        auto etrue = xAOD::TruthHelpers::getTruthParticle(*elPtr);
        if ( etrue != nullptr ) {
          m_el_true_pdg[i] = etrue->pdgId();
          m_el_true_pt[i]  = etrue->pt();
          m_el_true_eta[i] = etrue->eta();
        }
      }
      i++;
    }
  }

  if (m_config->useMuons()) {
    m_mu_truthMatched.resize(event.m_muons.size());
    m_mu_true_pdg.resize(event.m_muons.size());
    m_mu_true_pt.resize(event.m_muons.size());
    m_mu_true_eta.resize(event.m_muons.size());

    unsigned int i(0);
    for ( const auto* muPtr : event.m_muons ) {
      if ( top::isSimulation(event) ) {
        int true_type    = 0;
        m_mu_true_pt[i]  = 0;
        m_mu_true_eta[i] = 0;
        m_mu_true_pdg[i] = 0;
        auto mutrack = muPtr->primaryTrackParticle();
        if ( mutrack != nullptr ) {
          if ( truthType.isAvailable(*mutrack) ) true_type = truthType(*mutrack);
        }

        m_mu_truthMatched[i] = ( true_type == MCTruthPartClassifier::ParticleType::IsoMuon );

        if ( mutrack != nullptr && truthParticleLink.isAvailable(*mutrack) ) {
          auto ptr = truthParticleLink(*mutrack);
          const xAOD::TruthParticle* truthmu = (ptr ? *ptr : 0);
          if ( truthmu != nullptr ) {
            m_mu_true_pt[i]  = truthmu->pt();
            m_mu_true_eta[i] = truthmu->eta();
            m_mu_true_pdg[i] = truthmu->pdgId();
          }
        }
      }
      i++;
    } //loop on muons
  }

  ///-- Let the base class do all the hard work --///
  top::EventSaverFlatNtuple::saveEvent(event);
}


void top::EventSaverAIDA::saveParticleLevelEvent(const top::ParticleLevelEvent& plEvent) {
  if ( !m_config->isMC() ) {
    return;
  }
  if ( !(topConfig()->doTopParticleLevel()) ) {
    return;
  }

  const xAOD::TruthParticleContainer* neutrinos = nullptr;
  top::check(evtStore()->retrieve(neutrinos,"TruthNeutrinos"),"Failed to retrieve Truth Neutrinos");
  m_nu_pt.resize(neutrinos->size());
  m_nu_eta.resize(neutrinos->size());
  m_nu_phi.resize(neutrinos->size());
  m_nu_origin.resize(neutrinos->size());
  unsigned int i = 0;
  for ( const auto& neutrino : *neutrinos ) {
    unsigned int origin = 0;
    if ( particleOrigin.isAvailable(*neutrino) ) {
      origin = particleOrigin(*neutrino);
    }
    else if ( classifierParticleOrigin.isAvailable(*neutrino) ) {
      origin = classifierParticleOrigin(*neutrino);
    }
    else {
      std::cerr << "Could not obtain MCTruthClassifier result decoration." << std::endl;
    }

    m_nu_pt[i]     = neutrino->pt();
    m_nu_eta[i]    = neutrino->eta();
    m_nu_phi[i]    = neutrino->phi();
    m_nu_origin[i] = origin;
    ++i;
  }

  EventSaverFlatNtuple::saveParticleLevelEvent(plEvent);
}

void top::EventSaverAIDA::finalize() {
  top::EventSaverFlatNtuple::finalize();
}
