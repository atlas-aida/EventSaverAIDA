#include <EventSaverAIDA/EventSaverAIDA.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

//for loading the object selection at run time
#pragma link C++ class top::EventSaverAIDA+;

#endif
