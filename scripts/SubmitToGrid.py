#!/usr/bin/env python
#
# This script configures the Grid job.
# Copied and modified from SingleTopAnalysis

import sys, os
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--suffix",dest="suffix",required=True,type=str)
parser.add_argument("--noAODmeta",dest="noAODmeta",action="store_true")
parser.add_argument("--AFII",dest="AFII",action="store_true")
parser.add_argument("--btaggingWP",dest="BTaggingWP",default="FixedCutBEff_77")
parser.add_argument("--systematics",dest="systematics",default="All")
parser.add_argument("--jetUncModel",dest="jetUncModel",default="21NP")
parser.add_argument("--datasets",dest="datasets",type=str,required=True,nargs="+")
parser.add_argument("--destSE",dest="destSE",type=str,default="")
parser.add_argument("--noSubmit",dest="noSubmit",action="store_true")
parser.add_argument("--outputLHAPDF",dest="outputLHAPDF",action="store_true")
parser.add_argument("--printAvailable",dest="printAvail",action="store_true")
opts = parser.parse_args(sys.argv[1:])

### import main grid submission script
import TopExamples.grid
from EventSaverAIDA import MC15c
from EventSaverAIDA import Data
import EventSaverAIDA

if opts.printAvail:
    for ds in TopExamples.grid.AvailableDatasets():
        print ds
    exit()

#copied from SingleTopAnalysis grid.py
def findUserNickname():
    nickname = os.popen("voms-proxy-info -all | grep nickname").read()
    nickname = nickname.split(' ')[4]
    print "Nickname is \'" + nickname + "\'"
    return nickname

### name of program to be executed
config = TopExamples.grid.Config()

config.gridUsername    = findUserNickname()
config.suffix          = opts.suffix
config.excludedSites   = 'ANALY_GOEGRID,RU_PROTVINO-IHEP,ANALY_UIO'
config.noSubmit        = opts.noSubmit
config.mergeType       = 'Default' #'None', 'Default' or 'xAOD'
config.memory          = ''

if opts.destSE:
    config.destSE = opts.destSE

configfileName = 'aida_config.txt'

EventSaverAIDA.generateConfig(configfileName,
    EventSaverAIDA.aida_options(UseAodMetaData=(not opts.noAODmeta),
                                 AFII=opts.AFII,
                                 BTaggingWP=opts.BTaggingWP,
                                 Systematics=opts.systematics,
                                 JetUncertainties_NPModel=opts.jetUncModel,
                                 outputLHAPDF=opts.outputLHAPDF)
)

config.settingsFile = configfileName
samples = TopExamples.grid.Samples(opts.datasets)
TopExamples.grid.submit(config,samples)






"""
### to specify name of storage element to which the output should be replicated
### if the username is in the production team, a default grid site will be used, which can be overriden by specifying a different one
#config.destSE          = 'AUSTRALIA-ATLAS_LOCALGROUPDISK" --long --nGBPerJob="5'
#config.destSE = 'MWT2_UC_LOCALGROUPDISK'
#here we add in some cheeky additional options

### take name of set of datasets from argument list
#names = sys.argv
#names.pop(0) # remove script name from argument list

### name of configuration file with cuts
### by default, it will be assessed from the name of the list of samples
### specifying it here will ovveride the default
runs = {'data':[], 'mcFS':[], 'mcAF2':[], 'mcFS_PDF':[]}

for name in names:
    if name[0:4]=='Data':
        runs['data'].append(name)
    else:
        #if 'AF2' in name:
        #    runs['mcAF2'].append(name)
        if 'PDF' in name:
            runs['mcFS_PDF'].append(name)
        else:
            runs['mcFS'].append(name)

## hard coding the cut files for now
#dataSettingsFile = 'EventSaverAIDA/aida_cuts/dil-cuts.txt'
#mcSettingsFile   = 'EventSaverAIDA/aida_cuts/dil-cuts.txt'

### create lists of samples and submit jobs
if len(runs['data'])>0:
    config.settingsFile    = dataSettingsFile
    samples = TopExamples.grid.Samples(runs['data'])
    TopExamples.grid.submit(config, samples)
if len(runs['mcFS'])>0:
    config.settingsFile    =  mcSettingsFile
    samples = TopExamples.grid.Samples(runs['mcFS'])
    TopExamples.grid.submit(config, samples)
#if len(runs['mcAF2'])>0:
#    af2SettingsFile = mcSettingsFile[0:-4]+'_af2.txt'
#    settings = 'IsAFII True\n'
#    os.system('echo -e "%s$(cat %s)" > %s'%(settings, mcSettingsFile, af2SettingsFile))
#    config.settingsFile    = af2SettingsFile
#    samples = TopExamples.grid.Samples(runs['mcAF2'])
#    TopExamples.grid.submit(config, samples)
if len(runs['mcFS_PDF'])>0:
    pdfSettingsFile = mcSettingsFile[0:-4]+'_pdf.txt'
    cmd= 'cp %s %s'%(mcSettingsFile , pdfSettingsFile)
    os.system(cmd)
    cmd= 'PDFInfo True\n'
    cmd+='LHAPDFSets PDF4LHC15_nlo_30\n'
    cmd+='LHAPDFEventWeights True\n'
    os.system('echo -e "%s$(cat %s)" > %s'%(settings, mcSettingsFile, pdfSettingsFile))
    config.settingsFile    = pdfSettingsFile
    samples = TopExamples.grid.Samples(runs['mcFS_PDF'])
    TopExamples.grid.submit(config, samples)
"""
