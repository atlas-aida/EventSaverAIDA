/** @file  EventSaverAIDA.h
 *  @brief top::EventSaverAIDA class header
 *  @class top::EventSaverAIDA
 *  @brief Extends top-xaod output from AnalysisTop
 *
 *  We use the top::EventSaverFlatNtuple base class to add more
 *  variables to the output of top-xaod runs.
 *
 *  @author Douglas Davis < ddavis@cern.ch >
 */

#ifndef EventSaverAIDA_EventSaverAIDA_h
#define EventSaverAIDA_EventSaverAIDA_h

#include "TopAnalysis/EventSaverFlatNtuple.h"

namespace top {

  class EventSaverAIDA : public top::EventSaverFlatNtuple {

  private:

    std::shared_ptr<top::TopConfig> m_config;

    unsigned int m_lumiBlock;
    float m_met_sumet;

    std::vector<char>   m_el_truthMatched;
    std::vector<int>    m_el_true_pdg;
    std::vector<float>  m_el_true_pt;
    std::vector<float>  m_el_true_eta;

    std::vector<char>   m_mu_truthMatched;
    std::vector<int>    m_mu_true_pdg;
    std::vector<float>  m_mu_true_pt;
    std::vector<float>  m_mu_true_eta;

    std::vector<float> m_nu_pt;
    std::vector<float> m_nu_eta;
    std::vector<float> m_nu_phi;
    std::vector<int>   m_nu_origin;

    ClassDef(EventSaverAIDA, 1);

  public:

    EventSaverAIDA();

    virtual ~EventSaverAIDA();

    /// Define new variables for saving
    virtual void initialize(std::shared_ptr<top::TopConfig> config, TFile* file,
                            const std::vector<std::string>& extraBranches) override;

    /// Example says this is used to keep ASG tools happy
    virtual StatusCode initialize() override;

    /// Where the new variables are set and saved per event
    virtual void saveEvent(const top::Event& event) override;

    /// Define new variables for particle level output
    // virtual void setupParticleLevelTreeManager(const top::ParticleLevelEvent& plEvent);
    // virtual void setupParticleLevelTreeManager();

    /// Where the new particle level variablse are set and saved per event
    virtual void saveParticleLevelEvent(const top::ParticleLevelEvent& plEvent) override;

    /// Wrapping things up
    virtual void finalize() override;

  };

}

#endif
