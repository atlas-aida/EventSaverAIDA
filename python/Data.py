import TopExamples.grid
import TopExamples.ami

#The runs listed below (150 runs) are those that are included in this
#GRL:
#data16_13TeV.periodAllYear_DetStatus-v83-pro20-15_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml.
#This GRL contains periods A-L11 and corresponds to an integrated
#luminosity of 33257.2 pb-1 +/-4.5%, calculated with the preliminary
#2016 tag OflLumi-13TeV-005. The uncertainty on the integrated
#luminosity is +/-4.5%, but it is +/-4.1% when combined with the 2016
#dataset - more details can be found in the LuminosityForPhysics twiki
#page.  Here is the list of period containers (only the runs contained
#in the GRL are included):
PC_16 = [
    'data16_13TeV.periodA.physics_Main.PhysCont.DAOD_{}.grp16_v01_p2950',
    'data16_13TeV.periodB.physics_Main.PhysCont.DAOD_{}.grp16_v01_p2950',
    'data16_13TeV.periodC.physics_Main.PhysCont.DAOD_{}.grp16_v01_p2950',
    'data16_13TeV.periodD.physics_Main.PhysCont.DAOD_{}.grp16_v01_p2950',
    'data16_13TeV.periodE.physics_Main.PhysCont.DAOD_{}.grp16_v01_p2950',
    'data16_13TeV.periodF.physics_Main.PhysCont.DAOD_{}.grp16_v01_p2950',
    'data16_13TeV.periodG.physics_Main.PhysCont.DAOD_{}.grp16_v01_p2950',
    'data16_13TeV.periodI.physics_Main.PhysCont.DAOD_{}.grp16_v01_p2950',
    'data16_13TeV.periodK.physics_Main.PhysCont.DAOD_{}.grp16_v01_p2950',
    'data16_13TeV.periodL.physics_Main.PhysCont.DAOD_{}.grp16_v01_p2950'
]

# The runs listed below (65 runs) are those that are included in this
# final 2015 GRL specific for these 20.7 samples:
# data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml.
# This GRL contains periods D-J5 and corresponds to an integrated
# luminosity of 3212.96 pb-1 +/-2.1%, calculated with the final 2015
# tag OflLumi-13TeV-005. The uncertainty on the integrated luminosity
# is +/-2.1%, but it is when combined with the 2016 dataset - more
# details can be found in the LuminosityForPhysics twiki page.  Here is
# the list of period containers (only the runs contained in the GRL are
# included):
PC_15 = [
    'data15_13TeV.periodD.physics_Main.PhysCont.DAOD_{}.grp15_v01_p2950',
    'data15_13TeV.periodE.physics_Main.PhysCont.DAOD_{}.grp15_v01_p2950',
    'data15_13TeV.periodF.physics_Main.PhysCont.DAOD_{}.grp15_v01_p2950',
    'data15_13TeV.periodG.physics_Main.PhysCont.DAOD_{}.grp15_v01_p2950',
    'data15_13TeV.periodH.physics_Main.PhysCont.DAOD_{}.grp15_v01_p2950',
    'data15_13TeV.periodJ.physics_Main.PhysCont.DAOD_{}.grp15_v01_p2950'
]

TopExamples.grid.Add('Data15_TOPQ1_PCs').datasets = [i.format('TOPQ1') for i in PC_15]
TopExamples.grid.Add('Data16_TOPQ1_PCs').datasets = [i.format('TOPQ1') for i in PC_16]
