class aida_options:
    def __init__(self,
                 UseAodMetaData=True,
                 AFII=False,
                 BTaggingWP="FixedCutBEff_77",
                 Systematics="All",
                 JetUncertainties_NPModel="21NP",
                 outputLHAPDF=False):
        self.UseAodMetaData           = UseAodMetaData
        self.AFII                     = AFII
        self.BTaggingWP               = BTaggingWP
        self.Systematics              = Systematics
        self.JetUncertainties_NPModel = JetUncertainties_NPModel
        self.outputLHAPDF             = outputLHAPDF

def generateConfig(fname,options):
    f = open(fname,"w")

    f.write("LibraryNames libTopEventSelectionTools libTopEventReconstructionTools libEventSaverAIDA\n")

    f.write("### Good Run List\n")
    f.write("GRLDir  GoodRunsLists\n")
    f.write("GRLFile data15_13TeV/20160720/physics_25ns_20.7.xml data16_13TeV/20170215/physics_25ns_20.7.xml\n")

    f.write("### Pile-up reweighting tool - this is now mandatory\n")
    f.write("### Now requires only PRWLumiCalcFiles\n")
    f.write("### No PRWDefaultChannel anymore\n")
    f.write("### The nominal mc15c PU distribution is now appended to PRWConfigFiles automatically\n")
    f.write("### Will hence be using mc15c_v2_defaults.NotRecommended.prw.root if nothing is provided for PRWConfigFiles\n")
    f.write("# PRWConfigFiles TopCorrections/PRW.410000.mc15c.r7725_r7676.root\n")
    f.write("PRWConfigFiles\n")
    f.write("PRWLumiCalcFiles GoodRunsLists/data15_13TeV/20160720/physics_25ns_20.7.lumicalc.OflLumi-13TeV-005.root GoodRunsLists/data16_13TeV/20170215/physics_25ns_20.7.lumicalc.OflLumi-13TeV-008.root\n")
    f.write("# no PRWDefaultChannel anymore\n")

    f.write("ElectronCollectionName Electrons\n")
    f.write("MuonCollectionName Muons\n")
    f.write("JetCollectionName AntiKt4EMTopoJets\n")
    f.write("LargeJetCollectionName None\n")
    f.write("LargeJetSubstructure None\n")
    f.write("TauCollectionName None\n")
    f.write("PhotonCollectionName None\n")

    f.write("TruthCollectionName TruthParticles\n")
    f.write("TruthJetCollectionName AntiKt4TruthWZJets\n")
    f.write("TopPartonHistory ttbar\n")
    f.write("TopParticleLevel True\n")
    f.write("TruthBlockInfo False\n")
    f.write("PDFInfo True\n")
    if options.outputLHAPDF:
        f.write("LHAPDFSets PDF4LHC15_nlo_30\n")
        f.write("LHAPDFEventWeights True\n")
    f.write("MCGeneratorWeights True\n")

    f.write("ObjectSelectionName top::ObjectLoaderStandardCuts\n")
    f.write("#OutputFormat top::EventSaverFlatNtuple\n")
    f.write("OutputFormat top::EventSaverAIDA\n")
    f.write("OutputEvents SelectedEvents\n")
    f.write("OutputFilename output.root\n")
    f.write("PerfStats No\n")

    f.write("Systematics {0}\n".format(options.Systematics))
    f.write("JetUncertainties_NPModel {0}\n".format(options.JetUncertainties_NPModel))
    f.write("JetUncertainties_BunchSpacing 25ns\n")

    f.write("ElectronID TightLH\n")
    f.write("ElectronIDLoose MediumLH\n")
    f.write("ElectronIsolation Gradient\n")
    f.write("ElectronIsolationLoose None\n")
    f.write("ElectronPt 27000\n")
    f.write("TruthElectronPt 27000\n")

    f.write("MuonQuality Medium\n")
    f.write("MuonQualityLoose Medium\n")
    f.write("MuonIsolation Gradient\n")
    f.write("MuonIsolationLoose None\n")
    f.write("MuonPt 27000\n")
    f.write("TruthMuonPt 27000\n")

    f.write("FakesControlRegionDoLooseMC False\n")
    f.write("OverlapRemovalLeptonDef Tight\n")
    f.write("ApplyElectronInJetSubtraction False\n")

    if options.UseAodMetaData: usemetastring = "True"
    else:                      usemetastring = "False"
    f.write("UseAodMetaData {0}\n".format(usemetastring))

    if options.AFII: af2string = "True"
    else:            af2string = "False"
    f.write("IsAFII {0}\n".format(af2string))

    f.write("BTaggingWP {0}\n".format(options.BTaggingWP))

    f.write("########################\n")
    f.write("### basic selection with mandatory cuts for reco level\n")
    f.write("########################\n")

    f.write("SUB BASIC\n")
    f.write("INITIAL\n")
    f.write("GRL\n")
    f.write("GOODCALO\n")
    f.write("PRIVTX\n")
    f.write("RECO_LEVEL\n")

    f.write("########################\n")
    f.write("### definition of the data periods\n")
    f.write("########################\n")

    f.write("SUB period_2015\n")
    f.write("RUN_NUMBER >= 276262\n")
    f.write("RUN_NUMBER <= 284484\n")

    f.write("SUB period_2016\n")
    f.write("RUN_NUMBER >= 297730\n")

    f.write("########################\n")
    f.write("### lepton trigger and offline cuts for reco-level selections\n")
    f.write("########################\n")

    f.write("SUB EL_2015\n")
    f.write(". BASIC\n")
    f.write(". period_2015\n")
    f.write("TRIGDEC HLT_e24_lhmedium_L1EM20VH HLT_e60_lhmedium HLT_e120_lhloose\n")
    f.write("EL_N 27000 >= 1\n")

    f.write("SUB EL_2016\n")
    f.write(". BASIC\n")
    f.write(". period_2016\n")
    f.write("TRIGDEC HLT_e26_lhtight_nod0_ivarloose HLT_e60_lhmedium_nod0 HLT_e140_lhloose_nod0\n")
    f.write("EL_N 27000 >= 1\n")

    f.write("SUB MU_2015\n")
    f.write(". BASIC\n")
    f.write(". period_2015\n")
    f.write("TRIGDEC HLT_mu20_iloose_L1MU15 HLT_mu50\n")
    f.write("MU_N 27000 >= 1\n")

    f.write("SUB MU_2016\n")
    f.write(". BASIC\n")
    f.write(". period_2016\n")
    f.write("TRIGDEC HLT_mu26_ivarmedium HLT_mu50\n")
    f.write("MU_N 27000 >= 1\n")

    f.write("SUB EM_2015\n")
    f.write(". BASIC\n")
    f.write(". period_2015\n")
    f.write("TRIGDEC HLT_e24_lhmedium_L1EM20VH HLT_e60_lhmedium HLT_e120_lhloose HLT_mu20_iloose_L1MU15 HLT_mu50\n")
    f.write("EL_N_OR_MU_N 27000 >= 1\n")

    f.write("SUB EM_2016\n")
    f.write(". BASIC\n")
    f.write(". period_2016\n")
    f.write("TRIGDEC HLT_e26_lhtight_nod0_ivarloose HLT_e60_lhmedium_nod0 HLT_e140_lhloose_nod0 HLT_mu26_ivarmedium HLT_mu50\n")
    f.write("EL_N_OR_MU_N 27000 >= 1\n")


    f.write("########################\n")
    f.write("### emu selections\n")
    f.write("########################\n")

    f.write("SUB emu_basic\n")
    f.write("EL_N 27000 >= 1\n")
    f.write("MU_N 27000 >= 1\n")
    f.write("TRIGMATCH\n")
    f.write("#EMU_OVERLAP\n")
    f.write("JETCLEAN LooseBad\n")
    f.write("#HT > 120000\n")
    f.write("#JET_N 25000 >= 1\n")
    f.write("#JET_N 25000 >= 2\n")
    f.write("EL_N 27000 == 1\n")
    f.write("MU_N 27000 == 1\n")
    f.write("#OS\n")
    f.write("MLL > 15000\n")
    f.write("#TRUTH_MATCH\n")
    f.write("#JET_N_BTAG FixedCutBEff_77 >= 1\n")
    f.write("EXAMPLEPLOTS\n")
    f.write("NOBADMUON\n")

    f.write("SELECTION emu_2015\n")
    f.write(". EM_2015\n")
    f.write(". emu_basic\n")
    f.write("SAVE\n")

    f.write("SELECTION emu_2016\n")
    f.write(". EM_2016\n")
    f.write(". emu_basic\n")
    f.write("SAVE\n")

    f.write("SELECTION emu_particle\n")
    f.write("PRIVTX\n")
    f.write("PARTICLE_LEVEL\n")
    f.write("EL_N_OR_MU_N 27000 >= 1\n")
    f.write(". emu_basic\n")
    f.write("SAVE\n")

    f.write("########################\n")
    f.write("### ee selections\n")
    f.write("########################\n")

    f.write("SUB ee_basic\n")
    f.write("EL_N 27000 >= 2\n")
    f.write("TRIGMATCH\n")
    f.write("JETCLEAN LooseBad\n")
    f.write("#MET > 60000\n")
    f.write("#JET_N 25000 >= 1\n")
    f.write("#JET_N 25000 >= 2\n")
    f.write("EL_N 27000 == 2\n")
    f.write("MU_N 27000 == 0\n")
    f.write("#OS\n")
    f.write("MLL > 15000\n")
    f.write("MLLWIN 80000 100000\n")
    f.write("#TRUTH_MATCH\n")
    f.write("#JET_N_BTAG FixedCutBEff_77 >= 1\n")
    f.write("EXAMPLEPLOTS\n")
    f.write("#JET_N_BTAG FixedCutBEff_77 > 1\n")
    f.write("NOBADMUON\n")

    f.write("SELECTION ee_2015\n")
    f.write(". EL_2015\n")
    f.write(". ee_basic\n")
    f.write("SAVE\n")

    f.write("SELECTION ee_2016\n")
    f.write(". EL_2016\n")
    f.write(". ee_basic\n")
    f.write("SAVE\n")

    f.write("SELECTION ee_particle\n")
    f.write("PRIVTX\n")
    f.write("PARTICLE_LEVEL\n")
    f.write("EL_N 27000 >= 1\n")
    f.write(". ee_basic\n")
    f.write("SAVE\n")

    f.write("########################\n")
    f.write("### mumu selections\n")
    f.write("########################\n")

    f.write("SUB mumu_basic\n")
    f.write("MU_N 27000 >= 2\n")
    f.write("TRIGMATCH\n")
    f.write("#EMU_OVERLAP\n")
    f.write("JETCLEAN LooseBad\n")
    f.write("#MET > 60000\n")
    f.write("#JET_N 25000 >= 1\n")
    f.write("#JET_N 25000 >= 2\n")
    f.write("MU_N 27000 == 2\n")
    f.write("EL_N 27000 == 0\n")
    f.write("#OS\n")
    f.write("MLL > 15000\n")
    f.write("MLLWIN 80000 100000\n")
    f.write("#TRUTH_MATCH\n")
    f.write("#JET_N_BTAG FixedCutBEff_77 >= 1\n")
    f.write("EXAMPLEPLOTS\n")
    f.write("#JET_N_BTAG FixedCutBEff_77 > 1\n")
    f.write("NOBADMUON\n")

    f.write("SELECTION mumu_2015\n")
    f.write(". MU_2015\n")
    f.write(". mumu_basic\n")
    f.write("SAVE\n")

    f.write("SELECTION mumu_2016\n")
    f.write(". MU_2016\n")
    f.write(". mumu_basic\n")
    f.write("SAVE\n")

    f.write("SELECTION mumu_particle\n")
    f.write("PRIVTX\n")
    f.write("PARTICLE_LEVEL\n")
    f.write("MU_N 27000 >= 1\n")
    f.write(". mumu_basic\n")
    f.write("SAVE\n")
