Grid submission scripts and config files needed to run TopAnalysis on
the grid for AIDA. To run, you should ensure that rucio, panda, and
pyami are configured:

    $ lsetup rucio panda pyami

To run the grid submission:

    $ EventSaverAIDA/scripts/SubmitToGrid.py [options]

The only required options is `--datasets`, where you list (space
separated) the names of the samples you'd like to process. These are
defined in the files `python/MC15c.py` and `python/Data.py`.


Other options include:

- `--noAODmeta` to set the `UseAodMetaData` option to False (default true)
- `--AFII` for running over fast sim samples (default false)
- `--btaggingWP` for defining the b tagging working point (default FixedCutBEff_77)
- `--systematics` for defining which systematics to run (default All)
- `--jetUncModel` for defining which jet uncertainty model to use (default NP21)
- `--destSE` for pointing grid output to a specific site
- `--noSubmit` for test runs when you don't want to submit the job
- `--outputLHAPDF` for getting outputting stuff related to PDF reweighting

Examples:

    $ EventSaverAIDA/SubmitToGrid.py --datasets Data15_TOPQ1_PCs Data16_TOPQ1_PCs TOPQ1_ttbar_PowPy8_FS --destSE MWT2_UC_LOCALGROUPDISK

    $ EventSaverAIDA/SubmitToGrid.py --AFII --datasets TOPQ1_ttbar_PowPy8_AFII TOPQ1_Wt_PowPy6_AFII

This will create a file called `aida_config.txt` in your directory,
its the config file sent to `top-xaod`, you can save it for reference
if you wish.